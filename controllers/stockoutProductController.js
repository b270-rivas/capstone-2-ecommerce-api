// MODELS
const Admin = require("../models/Admin");
const Customer = require("../models/Customer");
const activeProducts = require("../models/activeProduct");
const stockoutProducts = require("../models/stockoutProduct");

const auth = require("../auth");

// CONTROLLER TO CREATE A PRODUCT (ADMIN ONLY)
module.exports.createStockoutProduct = (req, res) => {

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// If user is not an admin, return error.
	if (!userData.isAdmin) {
		return res.send({ error: 'Unauthorized.' });
	}

	// OBJECT DESTRUCTURING
	const { productISBN, productName, description, price, stocks } = req.body;

	// Use `Promise.all` to handle parallel execution of two asynchronous operations.
	Promise.all([

		// Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
		activeProducts.findOne({ productISBN: productISBN }),
		stockoutProducts.findOne({ productISBN: productISBN })
		])

	.then(([existingActiveProduct, existingStockoutProduct]) => {

		// If product exists in both collections, return error.
		if (existingActiveProduct || existingStockoutProduct) {
			return res.send({ error: 'Product already exists.' });
		}

		// If user is admin, create the new stockout product.
		const newStockoutProduct = new stockoutProducts({
			productISBN: productISBN,
			productName: productName,
			description: description,
			price: price,
			stocks: 0,
		});

		// Save `newStockoutProduct` to the `stockoutproduct` collection.
		return newStockoutProduct.save()
		.then(product => {

			// Return the new product detail in the terminal
			console.log(product);
			res.send(true);
		})
		// If `newStockoutProduct` was not saved to the `stockoutproduct` collection successfully, return error.
		.catch(error => {
			console.log(error);

			// Return false in Postman
			res.send(false);
		});
	})

	// Additional error handling
	.catch(error => {
		console.log(error);
		res.send(false);
	});
};

// CONTROLLER TO RETRIEVE A PRODUCT
module.exports.getProduct = (req, res) => {

	Promise.all([

		// Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
		activeProducts.findOne({ productISBN: req.params.productISBN }),
		stockoutProducts.findOne({ productISBN: req.params.productISBN })
		])

	.then(([activeProductFound, stockoutProductFound]) => {

		// If product exists in the `activeproduct` collection, return product detail.
		if (activeProductFound) {
			return res.send(activeProductFound);

		// If product exists in the `stockoutproduct` collection, return product detail.
		} else if (stockoutProductFound) {
			return res.send(stockoutProductFound);

		// If product does not exist in both collections, return error.
		} else {
			return res.send({ error: 'Product does not exist.' });
		}
	})

	// Additional error handling
	.catch(error => {
		console.log(error);
		res.send(false);
	});
};

// CONTROLLER TO UPDATE A STOCKOUT PRODUCT (ADMIN ONLY)
module.exports.updateStockoutProduct = (req, res) => {

    const { productISBN, productName, description, price, stocks } = req.body;

    // TOKEN AUTHENTICATION
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    // If user is not an admin, return error.
    if (!userData.isAdmin) {
        return res.send({ error: 'Unauthorized.' });
    }

    // If req.params.productISBN is not the same as the req.body.productISBN, return error.
    if (req.params.productISBN !== req.body.productISBN) {
        return res.send({
            error: 'Product ISBN in the URL does not match with the Product ISBN in the body.',
        });
    }

    Promise.all([

        // Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
        activeProducts.findOne({ productISBN: req.params.productISBN }),
        stockoutProducts.findOne({ productISBN: req.params.productISBN }),
        ])
    .then(([existingActiveProduct, existingStockoutProduct]) => {

        // If product does not exist in both collections, return error.
        if (!existingActiveProduct && !existingStockoutProduct) {
            return res.send({ error: 'Product does not exist.' });
        }

        // If the user is an admin, initialize `updatedProduct` with the required fields.
        const updatedProduct = {
            productISBN: req.body.productISBN,
            productName: req.body.productName || existingActiveProduct.productName,
            description: req.body.description || existingActiveProduct.description,
            price: req.body.price || existingActiveProduct.price,
            stocks: 0,
            isActive: false,
        };

        // Find the productISBN and update it with `updatedProduct`
        return stockoutProducts
        .findOneAndUpdate({ productISBN: req.params.productISBN }, updatedProduct)
        .then((product) => {
            console.log(product);
            res.send(true);
        })

        // If update is not successful, return error.
        .catch((error) => {
            console.log(error);
            res.send(false);
        });
    })
    
    // Additional error handling
    .catch((error) => {
        console.log(error);
        res.send(false);
    });
};

// CONTROLLER TO UNARCHIVE A STOCKOUT PRODUCT (ADMIN ONLY)
module.exports.unarchiveProduct = (req, res) => {
  // TOKEN AUTHENTICATION
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);

  // If user is not an admin, return error.
  if (!userData.isAdmin) {
    return res.send({ error: 'Unauthorized.' });
  }

  // Find productISBN in the `stockoutproduct` collection.
  stockoutProducts
    .findOne({ productISBN: req.params.productISBN })
    .then((existingStockoutProduct) => {
      // If product does not exist in the `stockoutproduct` collection, return error.
      if (!existingStockoutProduct) {
        return res.send({ error: 'Product does not exist.' });
      }

      // If the product has stocks > 0, it cannot be unarchived.
      if (existingStockoutProduct.stocks > 0) {
        return res.send({ error: 'Product cannot be unarchived since it already has stocks.' });
      }

      // If the product has stocks === 0, it can be unarchived.
      // Update the stocks to the value provided in the request body and isActive to true
      const updatedProduct = {
        productName: existingStockoutProduct.productName,
        description: existingStockoutProduct.description,
        price: existingStockoutProduct.price,
        stocks: req.body.stocks, // Use the value provided in the request body
        isActive: true,
      };

      // Find the productISBN and update it with `updatedProduct`
      return stockoutProducts
        .findOneAndUpdate({ productISBN: req.params.productISBN }, updatedProduct)
        .then((product) => {
          // Return the unarchived product detail in the terminal
          console.log(product);

          // Return true in Postman
          res.send(true);
        })
        .catch((error) => {
          // If update is not successful, return error.
          console.log(error);
          res.send(false);
        });
    })
    .catch((error) => {
      // Additional error handling
      console.log(error);
      res.send(false);
    });
};