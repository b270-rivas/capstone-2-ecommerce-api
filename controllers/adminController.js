// MODELS
const Admin = require("../models/Admin");
const Customer = require("../models/Customer");
const Orders = require("../models/Orders");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// CONTROLLER FOR USER REGISTRATION
module.exports.registerAdmin = (req, res) => {

	// OBJECT DESTRUCTURING
	const { firstName, lastName, userName, email, password, isAdmin, mobileNo } = req.body;

	// Find duplicate email in the `admin` db collection  
	Admin.findOne({ email: email })
	.then(existingUser => {

		// If duplicate found, return error.
		if (existingUser) {
			res.send({ error: 'User already exists.' });

		// If no duplicate, initialize `newAdmin` with the required fields.
		} else {
			let newAdmin = new Admin({
				firstName: firstName,
				lastName: lastName,
				userName: userName,
				email: email,
				password: bcrypt.hashSync(password, 10),
				isAdmin: true,
				mobileNo: mobileNo
			});

			// Save `newAdmin` to the `admin` collection and return confirmation.
			newAdmin.save().then(user => {
				console.log(user);
				res.send({ message: `Admin registration successful.`});
			})

			// If `newAdmin` was not saved to the `admin` collection successfully, return error.
			.catch(error => {
				console.log(error);
				res.send({ message: `User registration failed.` });
			});
		}
	});
};

// CONTROLLER FOR USER AUTHENTICATION
module.exports.loginAdmin = (req, res) => {

	// OBJECT DESTRUCTURING
	const { email, password } = req.body;

	// Check the email in the `admin` collection
	Admin.findOne({ email })
	.then((admin) => {

		// If admin, return admin.
		if (admin) {
			return admin;

		// If not admin, check the email in the `customer` collection.
		} else {
			return Customer.findOne({ email });
		}
	})
	.then((user) => {

		// If email is not found in both collections, return error.
		if (!user) {
			return res.send({ error: "User not found" });
		}

		// Check if password is correct
		const isPasswordCorrect = bcrypt.compareSync(password, user.password);

		// If password is incorrect, return error.
		if (!isPasswordCorrect) {
			return res.send({ error: "Password is incorrect" });
		}

		// User found, create and send JWT token
		const accessToken = auth.createAccessToken(user);
		return res.send({ accessToken });
	})

	// Error handling
	.catch((err) => {
		console.log(err);
		return res.send(false);
	});
};

// CONTROLLER TO RETRIEVE USER DETAILS
module.exports.adminDetails = (req, res) => {
// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

// Find email in the `admin` and `customer` db collection
	return Promise.all([
		Admin.findOne({ email: req.body.email }),
		Customer.findOne({ email: req.body.email })
		])
	.then(([existingAdmin, existingCustomer]) => {
// If email is not in both collections, return error.
		if (!existingAdmin && !existingCustomer) {
			return res.send({ error: 'User does not exist.' });
		}

// Choose the existing user object based on the collection it was found in
		const result = existingAdmin || existingCustomer;

// Change the value of the user's password to an empty string when returned to the frontend
		result.password = '';
		return res.send(result);
	})
// Error handling
	.catch(error => {
		console.log(error);
		return res.send({ error: 'Failed to retrieve user details.' });
	});
};

// CONTROLLER FOR RETRIEVE ALL ORDERS (ADMIN ONLY)
module.exports.allOrders = (req, res) => {

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// If user is not an admin, return error.
	if (!userData.isAdmin) {
		return res.send({ error: 'Unauthorized.' });

	// If user is admin, retrieve all orders
	} else {
		return Orders.find()
			.then(result => res.send(result))

			// Error handling
			.catch(err => {
				console.log(err);
				return res.send(false);
			});
	}
};

// CONTROLLER TO SET USER AS ADMIN (ADMIN ONLY)
module.exports.setAdmin = (req, res) => {

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// If user is not admin, return error.
	if (!userData.isAdmin) {
		return res.send({ error: 'Unauthorized.' });
	}

	const { email, isAdmin } = req.body

	// Use `Promise.all` to handle parallel execution of two asynchronous operations.
	Promise.all([

		// Find email in the `admin` and `customer` db collection  
		Admin.findOne({ email: req.body.email }),
		Customer.findOne({ email: req.body.email })
	])
	.then(([existingAdmin, existingCustomer]) => {

		// If email is not in both collection, return error.
		if (!existingAdmin && !existingCustomer) {
			return res.send({ error: 'User does not exist.' });
		}

		// If email is found in `customer` collection, initialize `newAdmin` with the required fields.
		if (existingCustomer) {
			const newAdmin = new Admin({
				firstName: existingCustomer.firstName,
				lastName: existingCustomer.lastName,
				userName: existingCustomer.userName,
				email: existingCustomer.email,
				password: bcrypt.hashSync(existingCustomer.password, 10),
				isAdmin: true,
				mobileNo: existingCustomer.mobileNo
			});

			// Save `newAdmin` to the `admin` collection.
			return newAdmin
				.save()
				.then(() => {

					// Delete user info from the `customer` collection.
					return Customer.findOneAndDelete({ email: req.body.email });
				})

				// If user has been set as admin successfully, return confirmation.
				.then(() => {
					return res.send({ message: `User has been set as admin.` });
				})

				// Error handling
				.catch(error => {
					console.log(error);
					return res.send({ error: `User is already an admin.` });
				});
		}
	})

	// Additional error handling
	.catch(error => {
		console.log(error);
		return res.send(false);
	});
};