// MODELS
const Admin = require("../models/Admin");
const Customer = require("../models/Customer");
const activeProducts = require("../models/activeProduct");
const stockoutProducts = require("../models/stockoutProduct");

const auth = require("../auth");

// CONTROLLER TO CREATE A PRODUCT (ADMIN ONLY)
module.exports.createActiveProduct = (req, res) => {

	// OBJECT DESTRUCTURING
	const { productISBN, productName, description, price, stocks } = req.body;

	// Use `Promise.all` to handle parallel execution of two asynchronous operations.
	Promise.all([

		// Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
		activeProducts.findOne({ productISBN: productISBN }),
		stockoutProducts.findOne({ productISBN: productISBN })
		])

	.then(([existingActiveProduct, existingStockoutProduct]) => {

		// If product exists in both collections, return error.
		if (existingActiveProduct || existingStockoutProduct) {
			return res.send({ error: 'Product already exists.' });
		}

		// TOKEN AUTHENTICATION
		const userData = auth.decode(req.headers.authorization);
		console.log(userData);

		// If user is not an admin, return error.
		if (!userData.isAdmin) {
			return res.send({ error: 'Unauthorized.' });
		}

		// If user is admin, initialize `newActiveProduct` with the required fields.
		const newActiveProduct = new activeProducts({
			productISBN: productISBN,
			productName: productName,
			description: description,
			price: price,
			stocks: stocks,
		});

		// Save `newActiveProduct` and return confirmation.
		return newActiveProduct.save()
		.then(product => {
			console.log(product);
			res.send({ message: 'Product creation successful.' });
		})

		// If `newActiveProduct` was not saved to the `activeproduct` collection successfully, return error.
		.catch(error => {
			console.log(error);
			res.send({ error: 'Product creation failed.' });
		});
	})

	// Additional error handling
	.catch(error => {
		console.log(error);
		res.send(false);
	});
};


// CONTROLLER TO RETRIEVE ALL ACTIVE PRODUCT
module.exports.allActiveProducts = (req, res) => {

	// Find from the `activeProducts` collection all products that are isActive: true.
	return activeProducts.find({isActive: true})

	// Return all active products
	.then(result => res.send(result));
}


// CONTROLLER TO RETRIEVE A PRODUCT
module.exports.getProduct = (req, res) => {

	Promise.all([

		// Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
		activeProducts.findOne({ productISBN: req.params.productISBN }),
		stockoutProducts.findOne({ productISBN: req.params.productISBN })
		])

	.then(([activeProductFound, stockoutProductFound]) => {

		// If productISBN is found from the `activeproduct` collection, return product detail.
		if (activeProductFound) {
			return res.send(activeProductFound);
		}

		// If productISBN is found from the `stockoutproduct` collection, return product detail.
		if (stockoutProductFound) {
			return res.send(stockoutProductFound);
		}

		// If productISBN is not found from either collections, return error.
		return res.send({ error: 'Product does not exist.' });
	})

	// Additional error handling
	.catch(error => {
		console.log(error);
		res.send(false);
	});
};


// CONTROLLER TO UPDATE AN ACTIVE PRODUCT (ADMIN ONLY)
module.exports.updateActiveProduct = (req, res) => {

	const { productISBN, productName, description, price, stocks } = req.body;

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// If user is not an admin, return error.
	if (!userData.isAdmin) {
		return res.send({ error: 'Unauthorized.' });
	}

	// If req.params.productISBN is not the same as the req.body.productISBN, return error.
	if (req.params.productISBN !== req.body.productISBN) {
		return res.send({
			error: 'Product ISBN in the URL does not match with the Product ISBN in the body.',
		});
	}

	Promise.all([

	// Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
		activeProducts.findOne({ productISBN: req.params.productISBN }),
		stockoutProducts.findOne({ productISBN: req.params.productISBN }),
		])
	.then(([existingActiveProduct, existingStockoutProduct]) => {

		// If product does not exist in both collections, return error.
		if (!existingActiveProduct && !existingStockoutProduct) {
			return res.send({ error: 'Product does not exist.' });
		}

		// If the user is an admin, initialize `updatedProduct` with the required fields.
		const updatedProduct = {
			productISBN: req.body.productISBN,
	        productName: req.body.productName || existingActiveProduct.productName,
	        description: req.body.description || existingActiveProduct.description,
	        price: req.body.price || existingActiveProduct.price,
			stocks: existingActiveProduct ? existingActiveProduct.stocks : req.body.stocks,
			isActive: true,
		};

		// Find the productISBN and update it with `updatedProduct`
		return activeProducts
		.findOneAndUpdate({ productISBN: req.params.productISBN }, updatedProduct)

		// If update is successful, return confirmation.
		.then((product) => {
			console.log(product);
			res.send({ message: `Update successful.` });
		})

		// If update is not successful, return error.
		.catch((error) => {
			console.log(error);
			res.send({ error: `Update failed.` });
		});
	})

	// Additional error handling
	.catch((error) => {
		console.log(error);
		res.send(false);
	});
};


// CONTROLLER TO ARCHIVE AN ACTIVE PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (req, res) => {
	const { productISBN } = req.body;

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// If user is not an admin, return error.
	if (!userData.isAdmin) {
		return res.send({ error: 'Unauthorized.' });
	}

	// If req.params.productISBN is not the same as the req.body.productISBN, return error.
	if (req.params.productISBN !== productISBN) {
		return res.send({
			error: 'Product ISBN in the URL does not match with the Product ISBN in the body.',
		});
	}

	Promise.all([

		// Find productISBN in the `activeproduct` and `stockoutproduct` db collection.
		activeProducts.findOne({ productISBN: req.params.productISBN }),
		stockoutProducts.findOne({ productISBN: req.params.productISBN }),
		])
	.then(([existingActiveProduct, existingStockoutProduct]) => {

		// If product does not exist in the `activeproduct` collection, return error.
		if (!existingActiveProduct) {
			return res.send({ error: 'Product does not exist.' });
		}

		// If product is already archived in the `stockoutproduct` collection, return error.
		if (existingStockoutProduct) {
			return res.send({ error: 'Product is already archived.' });
		}

		// If the product has stocks <= 0, it cannot be archived.
		if (existingActiveProduct.stocks <= 0) {
			return res.send({ error: 'Product stocks should be greater than 0 to be archived.' });
		}

		// Create the document in the `stockoutProducts` collection with existing stock value.
		const stockoutProductData = {
			productISBN: existingActiveProduct.productISBN,
			productName: existingActiveProduct.productName,
			description: existingActiveProduct.description,
			price: existingActiveProduct.price,
			stocks: 0,
		};

		// Move the product to the `stockoutProducts` collection and return confirmation.
		return stockoutProducts
		.create(stockoutProductData)
		.then((product) => {
			console.log(product);


			return activeProducts
			.findOneAndDelete({ productISBN: req.params.productISBN })
			.then(() => {
				res.send({ message: 'Product has been archived.' });
			})

			// If archiving failed, return error.
			.catch((error) => {
				console.log(error);
				res.send({ error: 'Archiving failed.' });
			});
		})
		.catch((error) => {
			console.log(error);
			res.send({ error: 'Archiving failed.' });
		});
	})
	.catch((error) => {
		console.log(error);
		res.send(false);
	});
};