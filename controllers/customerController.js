// MODELS
const Admin = require("../models/Admin");
const Customer = require("../models/Customer");
const Orders = require("../models/Orders");
const activeProducts = require("../models/activeProduct");
const stockoutProducts = require("../models/stockoutProduct");

const bcrypt = require("bcrypt");
const auth = require("../auth");


// CONTROLLER TO USER REGISTRATION
module.exports.registerCustomer = (req, res) => {

	// OBJECT DESTRUCTURING
	const { firstName, lastName, userName, email, password, mobileNo } = req.body;

	// Find duplicate email in the `customer` db collection
	Customer.findOne({ email: req.body.email })
	.then(existingUser => {

		// If duplicate found, return error.
		if (existingUser) {
			res.status(400).send({ error: 'User already exists.' });

		// If email is not found, initialize `newCustomer` with the required fields.
		} else {
			let newCustomer = new Customer({
				firstName: firstName,
				lastName: lastName,
				userName: userName,
				email: email,
				password: bcrypt.hashSync(password, 10),
				isAdmin: false,
				mobileNo: mobileNo
			});

			// Save `newCustomer` to the `customer` collection and return confirmation.
			newCustomer.save()
			.then(user => {
				console.log(user);
				res.send({ message: `Customer registration successful.`});
			})

			// If `newCustomer` was not saved to the `admin` collection successfully, return error.
			.catch(error => {
				console.log(error);
				res.send({ message: `User registration failed.` });
			});
		}
	});
};

// CONTROLLER TO USER AUTHENTICATION
module.exports.loginCustomer = (req, res) => {

	// OBJECT DESTRUCTURING
	const { email, password } = req.body;

	// Check the email in the `customer` collection
	Customer.findOne({ email: req.body.email })
	.then((customer) => {

		// If customer, return customer.
		if (customer) {
			return customer;

		// If not customer, check the email in the `admin` collection.
		} else {
			return Admin.findOne({ email: req.body.email });
		}
	})
	.then((user) => {

		// If email is not found in both collections, return error.
		if (!user) {
			return res.send({ error: "User not found" });
		}

		// Check if password is correct
		const isPasswordCorrect = bcrypt.compareSync(password, user.password);

		// If password is incorrect, return error.
		if (!isPasswordCorrect) {
			return res.send({ error: "Password is incorrect" });
		}

		// User found, create and send JWT token
		const accessToken = auth.createAccessToken(user);
		return res.send({ accessToken });
	})

	// Error handling
	.catch((err) => {
		console.log(err);
		return res.send(false);
	});
};

// CONTROLLER TO CREATE ORDER (NON-ADMIN ONLY)
module.exports.order = async (req, res) => {

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// If user is admin, return error.
	if (userData.isAdmin) {
		return res.send({ error: 'Only non-admins can place an order.' });
	}

	// OBJECT DESTRUCTURING
	const { productISBN, productName } = req.body;

	try {

		// Check if the product is active or out-of-stock
		const [existingActiveProduct, existingStockoutProduct] = await Promise.all([
			activeProducts.findOne({ productISBN: req.body.productISBN }),
			stockoutProducts.findOne({ productISBN: req.body.productISBN })
		]);

		// If the product is out-of-stock, return error.
		if (existingStockoutProduct) {
			return res.send({ error: 'Product is out-of-stock.' });
		}

		// Initialize `data` with user and product information
		const data = {
			userId: userData.id,
			email: userData.email,
			productISBN: productISBN,
			productName: productName
		};
		console.log(data);

		// Retrieve user from the `customer` collection
		const customerUpdated = await Customer.findById(data.userId)

		// Update and push the order details to the user's `order`
		.then(customer => {
			customer.order.push({
				productISBN: data.productISBN,
				productName: data.productName
			});

			// Save the updated `order` details and return true.
			return customer
				.save()
				.then(result => {
					console.log(result);
					return true;
				})

				// If `order` details was not saved successfully, return false.
				.catch(error => {
					console.log(error);
					return false;
				});
		});
		console.log(customerUpdated);

		// Retrieve product from the `activeproducts` collection
		const productUpdated = await activeProducts.findOne({ productISBN: data.productISBN })
		.then(product => {

			// Decrease the stock count of the product by 1
			product.stocks -= 1;

			// Save the updated product details and return true.
			return product
				.save()
				.then(result => {
					console.log(result);
					return true;
				})

				// If product details was not saved successfully, return false.
				.catch(error => {
					console.log(error);
					return false;
				});
		});
		console.log(productUpdated);

		// If both customer and product are updated successfully, create an `order` with the required details.
		if (customerUpdated && productUpdated) {
			const newOrder = new Orders({
				email: data.email,
				productISBN: data.productISBN
			});

			// Save the `newOrder` to the `orders` collection and return 
			newOrder.save()
				.then(result => {
					console.log(result);
					return res.send({ message: 'Order confirmed.' });
				})

				// If `newOrder` was not saved successfully, return error.
				.catch(error => {
					console.log(error);
					return res.send({ error: 'Order failed.' });
				});
		} else {
			return res.send(false);
		}
	} catch (error) {
		console.log(error);
		return res.send(false);
	}
};

// CONTROLLER TO RETRIEVE USER DETAILS
module.exports.customerDetails = (req, res) => {
	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// Find email in the `admin` and `customer` db collection
	return Promise.all([
		Admin.findOne({ email: req.body.email }),
		Customer.findOne({ email: req.body.email })
		])
	.then(([existingAdmin, existingCustomer]) => {
	// If email is not in both collections, return error.
		if (!existingAdmin && !existingCustomer) {
			return res.send({ error: 'User does not exist.' });
		}

	// Choose the existing user object based on the collection it was found in
		const result = existingAdmin || existingCustomer;

	// Change the value of the user's password to an empty string when returned to the frontend
		result.password = '';
		return res.send(result);
	})
	// Error handling
	.catch(error => {
		console.log(error);
		return res.send({ error: 'Failed to retrieve user details.' });
	});
};

// CONTROLLER TO RETRIEVE CUSTOMER ORDERS
module.exports.myOrders = (req, res) => {

	// TOKEN AUTHENTICATION
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// Find all orders of the user in the `order` collection
	Orders.findOne({ email: req.body.email })
	.then(orderList => {

		// If order is found, return the order.
		if (orderList) {
			return res.send(orderList);
		} else {
			return res.send({ error: `No order found.`})
		}
	})

	// Error handling
	.catch(error => {
		console.log(error);
		return res.send(false);
	});
};