const express = require("express");
const mongoose = require("mongoose");

// ROUTES
const adminRoutes = require("./routes/adminRoutes");
const customerRoutes = require("./routes/customerRoutes");
const activeProductRoutes = require("./routes/activeProductRoutes");
const stockoutProductRoutes = require("./routes/stockoutProductRoutes");

const cors = require("cors");
const app = express();

// MongoDB CONNECTION
mongoose.connect("mongodb+srv://admin:admin123@zuitt.sknrqtl.mongodb.net/b270_capstone_2?retryWrites=true&w=majority", 
{
useNewUrlParser : true, // Avoid any current and future errors while connecting to MongoDB
useUnifiedTopology : true // Connect to MongoDB even if the required url is updated
} 
);

// ERROR HANDLING
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error")); // If a connection error occured, error is printed in the terminal.
db.once("open", () => console.log("We're connected to the cloud database")) //If the connection is successful, "We're connected to the cloud database" is printed in the terminal

// MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Middleware function to check if user is an ADMIN or CUSTOMER
const adminPrivileges = (req, res, next) => {
    if (req.user && req.user.isAdmin) {
// If admin, route the request to the adminRoutes
        next();
    } else {
// If non-admin, route the request to the customerRoutes
        next('route');
    }
};

// Middleware function to check if product is an ACTIVE or STOCKOUT
const checkAvailability = (req, res, next) => {
    if (req.product && req.product.isActive) {
// If isActive, route the request to the activeProductRoutes
        next();
    } else {
// If not, route the request to the stockoutProductRoutes
        next('route');
    }
};

// checkAvailability is executed first so that the availability of the product is checked regardless of the authentication status.
app.use(checkAvailability);
// If adminPrivileges is executed first, the request will be routed to the customerRoutes immediately without performing the availability check.
app.use(adminPrivileges);

// FINAL ROUTE HANDLERS
app.use("/user", adminRoutes);
app.use("/user", customerRoutes);
app.use("/product", activeProductRoutes);
app.use("/product", stockoutProductRoutes);


// LISTENING PORT
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
});