const express = require('express');
const router = express.Router();
const auth = require("../auth");

// CONTROLLER
const adminController = require('../controllers/adminController');
const customerController = require('../controllers/customerController');

// ROUTE FOR USER REGISTRATION
router.post('/register', (req, res) => {

	// Extract the relevant fields from the request body using object destructuring.
	const { firstName, lastName, userName, email, password, isAdmin, mobileNo } = req.body;

	// If isAdmin is true, proceed to registerAdmin.
	if (isAdmin === true) {
		adminController.registerAdmin(req, res);

	// If isAdmin is false, proceed to registerCustomer.
	} else {
		customerController.registerCustomer(req, res);
	}
});

// ROUTE FOR USER AUTHENTICATION
router.post('/login', adminController.loginAdmin);

// ROUTE FOR RETRIEVE USER DETAILS
router.get('/details', auth.verify, (req, res) => {

	// Object destructuring
	const { firstName, lastName, userName, email, password, isAdmin, mobileNo } = req.body;

	// If isAdmin is true, proceed to adminDetails.
	if (isAdmin === true) {
		adminController.adminDetails(req, res);

	// If isAdmin is false, proceed to customerDetails.
	} else {
		customerController.customerDetails(req, res);
	}
});

// ROUTE FOR RETRIEVE ALL ORDERS (ADMIN ONLY)
router.get('/orders', auth.verify, adminController.allOrders);

// ROUTE TO SET USER AS ADMIN (ADMIN ONLY)
router.put('/setAsAdmin', auth.verify, adminController.setAdmin)

module.exports = router;