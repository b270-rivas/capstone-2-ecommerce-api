const express = require("express");
const router = express.Router();
const auth = require("../auth");

// CONTROLLER
const activeProductController = require("../controllers/activeProductController");
const stockoutProductController = require("../controllers/stockoutProductController");

// ROUTE TO CREATE PRODUCT (ADMIN ONLY)
router.post('/create', auth.verify, (req, res) => {

	// Extract the relevant fields from the request body using object destructuring.
	const { productISBN, productName, description, price, stocks } = req.body; 

	// If stocks is 0, proceed to createStockoutProduct.
	if (stocks === 0) {
		stockoutProductController.createStockoutProduct(req, res);

	} else {
	// If stocks is not 0, proceed to createActiveProduct.
		activeProductController.createActiveProduct(req, res);
	}
});

// ROUTE TO RETRIEVE ALL ACTIVE PRODUCT
router.get('/active', activeProductController.allActiveProducts);

// ROUTE TO RETRIEVE A PRODUCT
router.get('/:productISBN', activeProductController.getProduct);

// ROUTE TO UPDATE A PRODUCT (ADMIN ONLY)
router.patch('/:productISBN/update', auth.verify, (req, res) => {
	
	const { productISBN, productName, description, price, stocks } = req.body;

	// If stocks is 0, proceed to updateStockoutProduct.
	if (stocks === 0) {
		stockoutProductController.updateStockoutProduct(req, res);

	// If stocks is not 0, proceed to updateActiveProduct.
	} else {
		activeProductController.updateActiveProduct(req, res);
	}
});

// ROUTE TO ARCHIVE A PRODUCT (ADMIN ONLY)
router.delete('/:productISBN/archive', auth.verify, activeProductController.archiveProduct);

module.exports = router;