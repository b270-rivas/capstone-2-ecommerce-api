const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	userName: {
		type: String,
		required: [true, "Username is required."]
	},
	email: {
	    type: String,
	    required: [true, "Email is required."],
	    unique: true
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		required: [true, "isAdmin is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	order: [{
		productISBN: {
			type: String,
			required: [true, "Product ISBN is required."]
		},
		orderedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Ordered"
		}
	}]
});

module.exports = mongoose.model('Customer', customerSchema);