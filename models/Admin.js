const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	userName: {
		type: String,
		required: [true, "Username is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."],
		unique: true // To prevent duplicates
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		required: [true, "isAdmin is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	createdOn: {
		type: Date,
	default: new Date()
	}
});

module.exports = mongoose.model('Admin', adminSchema);