const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({	
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	productISBN: {
		type: String,
		required: [true, "ISBN is required"]
	},
	isPaid: {
		type: Boolean,
	default: true
	},
	dateOrdered: {
		type: Date,
	default: new Date()
	}
});

module.exports = mongoose.model("Orders", orderSchema);