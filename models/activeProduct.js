const mongoose = require("mongoose");

const activeProductSchema = new mongoose.Schema({
	productISBN: {
		type: String,
		required: [true, "ISBN is required"]
	},
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Stock is required"],
		validate: {

			// Ensure that the stocks field can only have a value greater than 0.
			validator: function(v) {
				return v > 0;
			}
		}
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("activeProduct", activeProductSchema);