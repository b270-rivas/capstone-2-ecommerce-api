const mongoose = require("mongoose");

const stockoutProductSchema = new mongoose.Schema({
	productISBN: {
		type: String,
		required: [true, "ISBN is required"]
	},
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		default: 0
	},
	isActive: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("stockoutProduct", stockoutProductSchema);